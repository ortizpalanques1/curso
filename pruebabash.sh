#!/bin/bash

# sysinfo_page - A script to produce an HTML file

title="Información sobre mi sistema"
fecha=$(date)
usuario=$(uname)
usuarioA=$(uname -a)
quienSoy=$(whoami)
espacioLibre=$(df)
memoria5=$(df | head -2 | tail -2 | tr -s ' ' | cut -f3 -d' ')
memoria6=$(df | head -2 | tail -2 | tr -s ' ' | cut -f4 -d' ')
memoria7=$(df | head -3 | tail -1 | tr -s ' ' | cut -f2 -d' ')
memoria8=$(df | head -3 | tail -1 | tr -s ' ' | cut -f3 -d' ')
memoria9=$(($memoria8 / $memoria7))
memoria10=200
memoria11=160



cat <<- _EOF_ > pruebabash.html
    <html>
    <head>
         <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>$title</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <script src=https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.1.1/Chart.js></script>
        <script src=https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.1.1/Chart.min.js></script>
        <style>
            #paginaPrincipal {
                width: 80%;
                margin:0 auto;
                background-color: #d3d699;
                border: solid 1px #061405;
                border-radius: 5px;
                min-height: 500px;
                padding-left: 5px; 
            };

        </style>
    </head>
    <body>
    <div id="paginaPrincipal">    
        <h1>$title</h1>
        <p>La fecha de hoy es $fecha</p>
        <p>El equipo en uso es: $usuario </p>
        <p>Con las siguientes especificaciones: $usuarioA</p>
        <p>El ususario actual es: $quienSoy </p>
        <p>$espacioLibre =$df</p>
        <p>En la unidad C la memoria total es: $memoria5</p>
        <p>En la unidad C la memoria utilizada es: $memoria6</p>
        <p>En la unidad Secundaria la memoria total es: $memoria7</p>
        <p>En la unidad Secundaria la memoria utilizada es: $memoria8</p>
        <p>$memoria9</p>
        <canvas id="myCanvas" width="256" height="256"></canvas>
        <script>
        $(document).ready(function(){
            var ctx = $("myCanvas").get(0).getcontext("2D");
            var data = [
                {
                    value: 200,
                    color: "red",
                    highlight: "lightskyblue",
                    label: "libre"
                },
                {
                    value: 160,
                    color: "blue",
                    highlight: "papayawhip",
                    label: "usada"   
                }
            ];
        });
        var piechart = new Chart(ctx).Pie(data);
    </script>
    </div>
    </body>
    </html>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </script>
    <script
    src="https://code.jquery.com/jquery-3.3.1.js"
    integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
    crossorigin="anonymous">
    </script>
    <script 
    src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous">
    </script>
_EOF_

print "Se ha actualizado el archivo pruebabash.html" 
##Documentación:
##https://www.youtube.com/watch?v=ZTJkLtQXEfY