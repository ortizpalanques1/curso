#1. Ubicar los archivos para la copia de seguridad
#en el directorio C:/xampp/htdocs/
#2. Ubicar el directorio de base de datos wordpress y
#volcarlo en el directorio a copiar.
#Recordar usar los permisos necesarios del administrador
#de la base de datos:
mysqldump -uroot -pa wordpress > /htdocs/wordpress.sql
#Hacer eso con las demás bases de datos.
#3. Pasar a hacer la sincronización.
##3.1. Conectarse con la máquina del servidor.
###3.1.1. Generar una clave en el promt:
ssh-keygen
##Y se genera el siguiente mensaje.
##"Generating public/private rsa key pair.
##Enter file in which to save the key (/c/Users/LUSSAC13/.ssh/id_rsa)"
##LUSSAC13@clase MINGW64 ~
##$ cd C:
##LUSSAC13@clase MINGW64 /c
##$ cd users
##LUSSAC13@clase MINGW64 /c/users
##$ cd lussac13
##LUSSAC13@clase MINGW64 /c/users/lussac13
##$ cd .ssh
##LUSSAC13@clase MINGW64 /c/users/lussac13/.ssh
##$ cat *pub
##Esto es la clave:
##ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKIzG4IsnvEmi1KLr1A9sIT1vmiZRIDqRy8ediHYtQu4oN2/qp6dp8bBOeIGsmWkrXRD6hHS2EZvE45X53VCjdE0GmhOD/nhvnK588pisx5E+S5LNnGXBJ/cN2pTL4NTzvA2tejwEx73QatHtouO1HXrMucmx+GtaCzuPYcG0+3owcAj1xPH3WNMYoMGpCCTi2fh4w/ZKaFvD3KofyRI0O28zFYnM27KEborEUxyPpJQS3bDaHNQ2t33qoW4JzPVuYPAwuinP1P6t3msggWbiuh2sYNcnNyVc1t1zQiy5EiCgKq4603X53XnjMMEmVAZcCOrs7B9QDtw4X7UNZ/pzb lussac13@clase
###3.1.2 Enviar la clave al servidor
ssh-copy-id user@192.168.105.85
##Luego pide el password y añade la key.
##3.2. Preparar el comando rsync
rsync -av ~/xampp/htdocs/ user@192.168.105.85:/var/www/Marco/
#Este code se guarda como: C:/users/lussac13/cronometro.sh
#4. Pasar a la orden de automatización con cron tab.
crontab -e
#Se abre la pantalla para editar.
#Se quiere guardar a la 12:00 de la noche de los lunes
0 0 * * 1 rsync -av >> /bin/bash C:/users/lussac13/cronometro.sh
#Se cierra el code de crontab
#5. Para ver qué sucede 
sudo journalctl -u cron -f #-f muestra activamente
#Documentación
#https://www.youtube.com/watch?v=QZJ1drMQz1A
#https://crontab.guru/




