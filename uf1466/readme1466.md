## seguridad.sh
1. Este programa debería guardar los copias de seguridad *.sql de las bases de datos clientes y wordpress en el archivo de mydocuments.
2. Luego se programará una tarea rsync de las carpetas mydocuments y la carpeta htdocs.
3. Finalmente, se escribe el comando crontab -e correspondiente para que estas tareas se realicen cada día a las 23:00 horas.