#!bin/bash
# Se pasan los archivos de mySql a mydocuments. Aquí están todos los documentos del usuario
# Los archivos .sql contiene todos los cambios de la base de datos.
mysqldump -uroot -psecret wordpress > ~/LUSSAC13/mydocuments/wordpress.sql
mysqldump -uroot -psecret clientes > ~/LUSSAC13/mydocuments/clientes.sql
# Con todas las carpetas ya en mydocuments, se procede al rsync en /backups
rsync -av ~/LUSSAC13/mydocuments/ ~/backups/
# Se archivan también los documentos de apache.
rsync -av ~/xampp/htdocs/ ~/backups/
# Se le da a este archivo un nombre:
# Seguridad.sh y se guarda en C:/seguridad.sh
#Se programa el crontab -e
# 0 23 * * * C:/seguridad.sh
# se cierra el code de crontab