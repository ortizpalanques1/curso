#!/bin/bash

# Variables a utilizar
# Se incluyen los comandos que lee el git bash para windows
title="Información sobre mi sistema"
fecha=$(date)
usuario=$(uname)
usuarioA=$(uname -a)
quienSoy=$(whoami)
espacioLibre=$(df) #no se usa, en su lugar se prefieren las líneas siguientes
memoria5=$(df | head -2 | tail -1 | tr -s ' ' | cut -f3 -d' ')
memoria6=$(df | head -2 | tail -1 | tr -s ' ' | cut -f4 -d' ')
memoria7=$(df | head -3 | tail -1 | tr -s ' ' | cut -f2 -d' ')
memoria8=$(df | head -3 | tail -1 | tr -s ' ' | cut -f3 -d' ')



#Este cat crea una página html que con el crontab se actualiza igualmente para su visualización en el servidor
cat <<- _EOF_ > c:/xampp/htdocs/pruebabash.html
    <html>
    <head>
         <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>$title</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <style>
            #paginaPrincipal {
                width: 80%;
                margin:0 auto;
                background-color: #d3d699;
                border: solid 1px #061405;
                border-radius: 5px;
                min-height: 500px;
                padding-left: 5px; 
            };

        </style>
    </head>
    <body>
    <div id="paginaPrincipal">    
        <h1>$title</h1>
        <p>La fecha de hoy es $fecha</p>
        <p>El equipo en uso es: $usuario </p>
        <p>Con las siguientes especificaciones: $usuarioA</p>
        <p>El ususario actual es: $quienSoy </p>
        <p>En la unidad C la memoria total es: $memoria5</p>
        <p>En la unidad C la memoria usada es: $memoria6</p>
        <p>En la unidad D la memoria disponible es: $memoria7</p>
        <p>En la unidaD D la memoria utilizada es: $memoria8</p>
    </div>
    </body>
    </html>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    </script>
    <script
    src="https://code.jquery.com/jquery-3.3.1.js"
    integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
    crossorigin="anonymous">
    </script>
    <script 
    src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous">
    </script>
_EOF_

print "Se ha actualizado el archivo pruebabash.html" 
#El archivo crontab para esta tarea sería
# en el promt: $ crontab -e
#Se abre la pantalla para editar.
#Se quiere actualizar cada minuto con el siguiente comando en el editor.
# * * * * * C:/users/LUSSAC13/curso/uf1465/pruebabash.sh
#Se cierra el code de crontab
