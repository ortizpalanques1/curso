# curso

Scripts del curso Desarrollador de Aplicaciones Web
Los archivos contenidos desarrollan varios ejercicios propuestos en el curso "Desarrollador de aplicaciones web" de la Comunidad de Madrid. Hasta este momento sólo están los del primer módulo.

1. La carpeta uf1465 incluye dos programas para informar sobre el servidor. pruebabash.sh y monitorsistema01.sh. pruebabash.sh genera un archivo html y monitorsistema01 se probó en google console.
2. La carpeta uf1466 incluye las tareas para realizar un backup programado de las bases de datos, docuemntos personales y documentos para la web.
3. Agenda4.html: Este script en html realiza las funcione de una pequeña agenda en la que que se pueden añadir las entradas de nombre, apellido, teléfono y email. Las entradas pueden ser borradas o editadas. Una de sus principales características es que los datos se guardan localmente en el "local storage". Adicionalmente, asigna un número único a cada entrada. La función "buscar" debe ser implementada.
2. copiaseguridad.sh: Aunque guardado como bash, es en realida dun archivo con las indicaciones para hacer una copia de seguridad de los archivos que el interesado seleccione. Comienza con la creación de la clave ssh y sigue con la creación del archivo rsync y finaliza con la creación del crontab correspondiente. 
3. gitclonepush.sh: Al igual que el anterior es un archivo de instrucciones. En este caso contiene lo necesario para bajar y subir archivos en gitlab.com.
4. index2.html: Versión anterior de Agenda4.html.5. monitorsistema01:Procrama para la monitorización del sistema en Bash. algunos comandos no son reconocidos por gitbash para windows o google console.
6. pruebabash.sh: Este archivo funciona con comandos de monitorización del sistema de windows y genera una página de html con esos datos. Se le debería adjuntar un crontab.

